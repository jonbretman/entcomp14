import java.rmi.Naming;
public class HelloServer {
    /**
     * Server program for the âĂĲHello, world!âĂİ example.
     *
     * @param argv The command line arguments which are ignored.
     */
    public static void main(String[] argv) {
        try {
            Naming.rebind("Hello", new HelloImpl());
            System.out.println("Hello Server is ready.");
        } catch (Exception e) {
            System.out.println("Hello Server failed:" + e);
        }
    }
}

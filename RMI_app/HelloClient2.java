import java.rmi.Naming;

public class HelloClient2 {
    /**
     * Client program for the âĂĲHello, world!âĂİ example.
     *
     * @param argv The command line arguments which are ignored.
     */
    public static void main(String[] argv) {
        try {
            HelloInterface hello = (HelloInterface) Naming.lookup("Hello");
            String result = hello.sayHello("Chelsea");
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("HelloClient exception: " + e);
        }
    }
}

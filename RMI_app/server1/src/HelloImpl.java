package server1.src;

import interface1.Address;
import interface1.HelloInterface;

import java.rmi.RemoteException;

/**
 * Remote implementation class
 */
public class HelloImpl implements HelloInterface {
    public HelloImpl() {
        System.out.println("HelloImpl constructor");
    }

    @Override
    public String sayHello(String name, Address address) throws RemoteException {
        System.out.println("HelloImpl sayHello()");
        return "Hello " + name + "!" + " Your zip code is " + address.getZip();
    }
}
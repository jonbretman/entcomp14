package interface2;

public interface Task<T> {
    T execute();
}
